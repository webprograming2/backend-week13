import { IsNotEmpty, Length, Min } from 'class-validator';

export class CreateProductDto {
  @Length(3, 64)
  @IsNotEmpty()
  name: string;

  @Min(5)
  @IsNotEmpty()
  price: number;
}
