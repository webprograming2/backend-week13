import { IsNotEmpty, Length } from 'class-validator';

export class CreateUserDto {
  @Length(6, 18)
  @IsNotEmpty()
  username: string;

  @Length(8, 30)
  @IsNotEmpty()
  password: string;
}
