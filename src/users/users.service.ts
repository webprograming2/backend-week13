import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepositrory: Repository<User>,
  ) {}

  create(createUserDto: CreateUserDto) {
    return this.userRepositrory.save(createUserDto);
  }

  findAll() {
    return this.userRepositrory.find();
  }

  async findOne(id: number) {
    const user = await this.userRepositrory.findOneBy({ id: id });
    if (!user) {
      throw new NotFoundException();
    }
    return user;
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.userRepositrory.findOneBy({ id: id });
    if (!user) {
      throw new NotFoundException();
    }
    const editedUser = { ...user, ...updateUserDto };
    return this.userRepositrory.save(editedUser);
  }

  async remove(id: number) {
    const user = await this.userRepositrory.findOneBy({ id: id });
    if (!user) {
      throw new NotFoundException();
    }
    return this.userRepositrory.softRemove(user);
  }
}
